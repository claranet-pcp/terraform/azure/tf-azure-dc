output "dc_primary_ip" {
  value = "${local.dc_primary_ip}"
}

output "dc_ips" {
  value = "${azurerm_network_interface.DC_network_interface.*.private_ip_address}"
}

output "dc_id" {
  value = "${azurerm_virtual_machine.DC_host.*.id}"
}
