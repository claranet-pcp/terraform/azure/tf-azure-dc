locals {
  os_custom_data = "${var.primary == "1" ? base64encode(data.template_file.deployprimarydomain.rendered) : base64encode(data.template_file.deploysecondarydomain.rendered)}"
  zones = "${var.primary == "1" ? 1 : 2}"

  dc_primary_ip = "${var.primary == "1" ? element(azurerm_network_interface.DC_network_interface.*.private_ip_address,0) : var.dcip}"
}