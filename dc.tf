resource "azurerm_network_interface" "DC_network_interface" {
  name                = "${var.resource_prefix}-interface"
  location            = "${var.azure_location}"
  resource_group_name = "${var.resource_group}"

  ip_configuration {
    name                          = "${var.resource_prefix}-ipconfig"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "${var.private_ip_address}"
  }

  tags {
    service       = "AD"
    terraform     = "true"
    environment   = "${var.environment}"
    configuration = "${var.configuration}"
  }
}

resource "azurerm_virtual_machine" "DC_host" {
  name                  = "${var.resource_prefix}"
  location              = "${var.azure_location}"
  resource_group_name   = "${var.resource_group}"
  network_interface_ids = ["${element(azurerm_network_interface.DC_network_interface.*.id, count.index)}"]
  vm_size               = "${var.vm_size}"                                                                        

  zones = ["${local.zones}"]

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "${var.sku}"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.resource_prefix}-os"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.machine_name}"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"

    custom_data = "${local.os_custom_data}"
  }

  os_profile_windows_config {
    provision_vm_agent        = true
    enable_automatic_upgrades = true
      timezone = "${var.timezone}"

     additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "AutoLogon"
      content      = "<AutoLogon><Password><Value>${var.admin_password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.admin_username}</Username></AutoLogon>"
    } 

    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "FirstLogonCommands"
      content      = "${data.template_file.first_logon.rendered}"
    } 
  }

  tags {
    service       = "AD"
    terraform     = "true"
    environment   = "${var.environment}"
    configuration = "${var.configuration}"
  }
}

resource "azurerm_virtual_machine_extension" "Malware_Protection_Extension_DC" {
  name                 = "Antimalware"
  location             = "${var.azure_location}"
  resource_group_name  = "${var.resource_group}"
  virtual_machine_name = "${azurerm_virtual_machine.DC_host.name}"
  publisher            = "Microsoft.Azure.Security"
  type                 = "IaaSAntimalware"
  type_handler_version = "1.5"

  settings = <<SETTINGS
  { 
    "AntimalwareEnabled": true, 
    "RealtimeProtectionEnabled": true, 
    "ScheduledScanSettings": { 
      "isEnabled": true, 
      "day": "1", 
      "time": "120", 
      "scanType": "Full" 
    }, 
    "Exclusions": { 
      "Extensions": ".mdf;.ldf", 
      "Paths": "c:\\windows;c:\\windows\\system32", 
      "Processes": "taskmgr.exe;notepad.exe"
    } 
  }
SETTINGS

}

resource "azurerm_virtual_machine_extension" "oms_ext" {
  name                       = "AzureMonitor"
  location                   = "${var.azure_location}"
  resource_group_name        = "${var.resource_group}"
  virtual_machine_name       = "${azurerm_virtual_machine.DC_host.name}"
  publisher                  = "Microsoft.EnterpriseCloud.Monitoring"
  type                       = "MicrosoftMonitoringAgent"
  type_handler_version       = "1.0"
  auto_upgrade_minor_version = true

  settings = <<SETTINGS
  {
    "workspaceId": "${var.oms_workspace_id}"
  }
SETTINGS

  protected_settings = <<SETTINGS
  {
    "workspaceKey": "${var.oms_workspace_key}"
  }
SETTINGS

}

resource "azurerm_virtual_machine_extension" "vmdiag" {
  name                 = "VirtualMachineDiagnostics"
  location             = "${var.azure_location}"
  resource_group_name  = "${var.resource_group}"
  virtual_machine_name = "${azurerm_virtual_machine.DC_host.name}"
  publisher            = "Microsoft.Azure.Diagnostics"
  type                 = "IaaSDiagnostics"
  type_handler_version = "1.5"

  settings = <<SETTINGS
  {
    "xmlCfg": "${base64encode(data.template_file.xml.rendered)}",
    "storageAccount": "${var.diagstorageaccountname}"
  }
SETTINGS

  protected_settings = <<SETTINGS
  {
    "storageAccountName": "${var.diagstorageaccountname}",
    "storageAccountKey": "${var.diagstorageaccountkey}"
  }
SETTINGS

}

data "template_file" "deployprimarydomain" {
  template = "${file("${path.module}/scripts/deployprimary.tmpl")}"

  vars {
    domain         = "${var.ad_domain}"
    adminpass      = "${var.admin_password}"
    domain_netbios = "${var.ad_domain_netbios}"
  }
}

data "template_file" "deploysecondarydomain" {
  template = "${file("${path.module}/scripts/deploysecondary.tmpl")}"

  vars {
    domain         = "${var.ad_domain}"
    adminpass      = "${var.admin_password}"
    domain_netbios = "${var.ad_domain_netbios}"
    DCIP           = "${var.dcip}"
  }
}

data "template_file" "first_logon" {
  template = "${file("${path.module}/scripts/firstlogoncommands.tmpl")}"
}


data "template_file" "xml" {
  template = "${file("${path.module}/scripts/windows-config.xml.tmpl")}"
}

resource "azurerm_virtual_machine_extension" "language" {
  name                 = "CustomScript-Language"
  location             = "${var.azure_location}"
  resource_group_name  = "${var.resource_group}"
  virtual_machine_name = "${azurerm_virtual_machine.DC_host.name}"
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.7"

  protected_settings = <<SETTINGS
  {
    "commandToExecute": "powershell -command \"[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String('${base64encode(data.template_file.language_ps1.rendered)}')) | Out-File -filepath language.ps1\" && powershell -ExecutionPolicy Unrestricted -File language.ps1"
  }
SETTINGS

}

data "template_file" "language_ps1" {
  template = "${file("${path.module}/scripts/language.ps1")}"
}



