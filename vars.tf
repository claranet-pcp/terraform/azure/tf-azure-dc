variable "customer" {}

variable "environment" {}

variable "configuration" {}

variable "resource_prefix" {
  default = ""
}

variable "azure_location" {}

variable "azure_location_short" {
  default = "ne"
}

variable "timezone"{
  default = ""
}

# Never A0!!! Maybe switch to F2 -only in certain regions
variable "vm_size" {
  default = "Standard_b2s"
}

variable "admin_username" {}

variable "admin_password" {}

variable "ad_domain" {}

variable "ad_domain_netbios" {}

variable "resource_group" {}

variable "subnet_id" {}
variable "private_ip_address" {}

variable "machine_name" {
  default = "ad"
}



variable "sku" {
  default = "2016-Datacenter-server-Core"
}

variable "availability_set" {}

variable "primary" {
  default = "0"
}

variable "dcip" {
  default = "0"
}

variable "oms_workspace_id" {}
variable "oms_workspace_key" {}

variable "diagstorageaccountname" {}

variable "diagstorageaccountkey" {}
